using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;

using Feed.Lib;
using Feed.Lib.Sindication;
using Feed.Lib.Stores;
using Feed.ViewModels;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Feed.Controllers {

    [Route("")]
    public class HomeController : Controller {
        private readonly IWebHostEnvironment _env;
        private readonly IMessageStore _store;
        private readonly FeedEngine _feedEngine;

        public HomeController(IMessageStore store, FeedEngine feedEngine, IWebHostEnvironment env) {
            _env = env;
            _store = store;
            _feedEngine = feedEngine;
        }

        [HttpGet("", Name = Names.Routes.TopIndex)]
        public IActionResult Index() {
            IndexVM model = new(
                User?.Identity?.Name ?? null,
                _store.Fetch(),
                GetFileHashes(GetFilesToHash())

                );

            return View(model);
        }

        [HttpGet("feed", Name = Names.Routes.FeedIndex)]
        public IActionResult Feed() {
            IEnumerable<SyndicationFeed> feeds = _feedEngine.FetchAllAsync();

            Dictionary<SyndicationFeed, FeedVM> map = new();

            foreach (SyndicationFeed sf in feeds) {
                map.Add(sf, new FeedVM(GetText(sf.Title), GetLink(sf.Links), sf.ImageUrl));
            }

            return View(feeds.SelectMany(f => f.Items)
                .Select(i => new FeedItemVM(map[i.SourceFeed], GetLatestDate(i), GetText(i.Title), GetLink(i.Links)))
                .Where(i => i.Timestamp > DateTimeOffset.Now.AddDays(-1))
                .OrderBy(i => i.Timestamp)
                );
        }

        private IDictionary<string, string> GetFileHashes(IEnumerable<string> files)
            => files.Select(s => (s, GetHashAsync(s)))
                    .AsParallel()
                    .Select(t => { t.Item2.Wait(); return (t.s, t.Item2.Result); })
                    .ToDictionary(p => Path.GetRelativePath(_env.WebRootPath, p.s), p => p.Item2);

        private async Task<string> GetHashAsync(string path) {
            using (SHA256 sha = SHA256.Create()) {
                using (FileStream stream = System.IO.File.OpenRead(path)) {
                    return Convert.ToBase64String(await sha.ComputeHashAsync(stream));
                }
            }
        }

        private IEnumerable<string> GetFilesToHash() =>
            Directory.GetFiles(MapPath("/"))
                .Where(s => s.EndsWith(".js") || s.EndsWith(".css"));

        private string MapPath(string path) => Path.Join(_env.WebRootPath, path);

        private static Uri GetLink(IEnumerable<SyndicationLink> links) => links.ElementAt(0).Uri;

        private static DateTimeOffset GetLatestDate(SyndicationItem item) => LatestDate(item.PublishDate, item.LastUpdatedTime);

        private static string GetText(TextSyndicationContent text) {
            return text.Type switch {
                _ => text.Text,
            };
        }

        private static DateTimeOffset LatestDate(DateTimeOffset a, DateTimeOffset b) => b > a ? b : a;
    }
}