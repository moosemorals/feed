﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Feed.Lib.Network;
using Feed.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;

using Serilog;

namespace Feed.Controllers {


    [Route("events")]
    [ApiController]
    public class SSEController : ControllerBase {

        private readonly ILogger _log = Log.ForContext<SSEController>();
        private readonly EventSource _eventSource;

        public SSEController(EventSource eventSource) {
            _eventSource = eventSource;
        }

        [HttpGet("")]
        public async Task SendEvents() {

            // Add commands from the event source to a queue
            BlockingCollection<Command> _queue = new();
            void handler(object? source, Command cmd) => _queue.Add(cmd);
            _eventSource.OutgoingEvent += handler;

            // Start the request going
            CancellationToken requestToken = HttpContext.RequestAborted;
            Response.StatusCode = StatusCodes.Status200OK;
            Response.ContentType = "text/event-stream";
            await Response.StartAsync(requestToken);

            while (true) {
                // Sleep until we either get a command or
                // it's time for a heartbeat
                using CancellationTokenSource timerSource = new();
                using CancellationTokenSource jointSource = CancellationTokenSource.
                    CreateLinkedTokenSource(requestToken, timerSource.Token);

                Timer timer = new Timer(x => timerSource.Cancel(), null, TimeSpan.FromSeconds(25), Timeout.InfiniteTimeSpan);

                try {
                    _log.Debug("Waiting for command, disconnect, or timeout");
                    Command cmd = _queue.Take(jointSource.Token);

                    _log.Debug("Command: Got a message");

                    await Response.WriteAsync(EventSource.FormatSSE(JsonConvert.SerializeObject(cmd)));
                    await Response.Body.FlushAsync();

                } catch (OperationCanceledException) {
                    if (requestToken.IsCancellationRequested) {
                        _log.Debug("Disconnect: Request closed");
                        return;
                    } else if (timerSource.IsCancellationRequested) {
                        _log.Debug("Timeout: Heartbeat");
                        await Response.WriteAsync(EventSource.Heartbeat);
                        await Response.Body.FlushAsync();
                    } else {
                        _log.Debug("Unexpected cancellation");
                        throw;
                    }
                } finally {
                    timer.Dispose();
                }
            }
        }

        [HttpPost("")]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> ReceiveMessageAsync() {

            string payload;
            using (StreamReader stream = new(Request.Body)) {
                payload = await stream.ReadToEndAsync();
            }
            if (!string.IsNullOrEmpty(payload)) {
                Command? cmd = JsonConvert.DeserializeObject<Command>(payload);
                if (cmd != null) {
                    _eventSource.ReceiveMessage(cmd);
                    return NoContent();
                }
            }
            return BadRequest();
        }
    }
}
