using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Common.Lib;

using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using Serilog;
using Serilog.Context;

namespace Feed.Lib.Cache {
    public class CachedObjectService<T> : IFetcher<T> {
        private readonly ILogger _log = Log.ForContext<CachedObjectService<T>>();
        private readonly IConfiguration _config;
        private readonly HttpClient _http;

        public Func<byte[], T>? ResponseConverter { get; set; }

        private record CachedObject(string Target, DateTimeOffset Expires, DateTimeOffset LastModified, string Payload);

        public CachedObjectService(HttpClient http, IConfiguration config) {
            _config = config;
            _http = http;
        }

        public async Task<T> Fetch(string target) { 
            if (ResponseConverter == null) {
                throw new NullReferenceException("Converter must be set before Fetch");
            } 

            using (LogContext.PushProperty("cacheTarget", target)) {
                CachedObject? cachedObject = await LoadFromDiskAsync(target);

                DateTimeOffset now = DateTimeOffset.Now;

                if (cachedObject == null || cachedObject.Expires < now) {
                    if (cachedObject != null) {
                        _log.Information("Cache expired at {cacheTargetExpiry}", cachedObject.Expires);
                    } else {
                        _log.Information("Cache not held");
                    }

                    HttpRequestMessage req = new(HttpMethod.Get, target);

                    req.Headers.Add("User-Agent", "osric.uk (Feed Engine)");
                    if (cachedObject != null) {
                        _log.Debug("Cache:  Last Modified {cacheTargetLastModified}", cachedObject.LastModified);
                        req.Headers.IfModifiedSince = cachedObject.LastModified;
                    }

                    HttpResponseMessage resp = await _http.SendAsync(req);

                    if (resp.StatusCode == HttpStatusCode.OK) {
                        _log.Debug("Cache successful GET", target);

                        cachedObject = new CachedObject(
                            target,
                            resp.Content.Headers.Expires ?? now,
                            resp.Content.Headers.LastModified ?? now,
                            Convert.ToBase64String(await resp.Content.ReadAsByteArrayAsync())
                        );

                        await SaveToDiskAsync(cachedObject);
                    } else if (resp.StatusCode == HttpStatusCode.NotModified) {
                        _log.Debug("Cache: Fetched but not modified");

                        // cachedFetch must be not-null here because we
                        // used it above to get the IfModifedSince header
                        if (cachedObject == null) {
                            throw new NullReferenceException("cachedFetch is null, but it really shouldn't be");
                        }

                    } else {
                        // Failure?
                        string content = await resp.Content.ReadAsStringAsync();
                        throw new Exception("Can't get forecast: " + content);
                    }
                } else {
                    _log.Debug("Cache: Doesn't expire untill {cacheExpires}", cachedObject.Expires);
                }

                return ResponseConverter(Convert.FromBase64String(cachedObject.Payload));
            }
        }

        private async Task<CachedObject?> LoadFromDiskAsync(string target) {
            string cacheFilePath = GetCacheFilePath(target);
            return File.Exists(cacheFilePath)
                ? JsonConvert.DeserializeObject<CachedObject>(
                    await File.ReadAllTextAsync(cacheFilePath, Encoding.UTF8),
                    new JsonSerializerSettings {
                        DateParseHandling = DateParseHandling.None,
                    })
                : null;
        }

        private void MakeCacheFolder() {
            string? cacheFolder = Path.GetDirectoryName(GetCacheFilePath("ignored"));
            if (!string.IsNullOrEmpty(cacheFolder)) {
                Directory.CreateDirectory(cacheFolder);
            } else {
                throw new Exception($"Can't create cache folder '{cacheFolder}'");
            }
        }

        private async Task SaveToDiskAsync(CachedObject cache) {
            MakeCacheFolder();

            string json = JsonConvert.SerializeObject(cache);
            string cacheFilename = GetCacheFilePath(cache.Target);

            await File.WriteAllTextAsync(cacheFilename, json);
            File.SetLastWriteTimeUtc(cacheFilename, cache.LastModified.UtcDateTime);
        }

        private string GetCacheFilePath(string path) {
            return Path.Join(
                _config[Static.ConfigSharedFolder],
                "cache",
                $"{HashString(path)}.json"
            );
        }

        private static string HashString(string input) {
            using SHA256 hasher = SHA256.Create();
            return WebEncoders.Base64UrlEncode(hasher.ComputeHash(Encoding.UTF8.GetBytes(input)));
        }

    }
}
