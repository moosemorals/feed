using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feed.Lib.Cache {
    public interface IFetcher<T> {
        Func<byte[], T>? ResponseConverter { get; set; }

        public Task<T> Fetch(string url);
    }
}
