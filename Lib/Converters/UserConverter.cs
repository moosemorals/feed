using System;
using System.Collections.Generic;

using Common.Models.Auth;

using Newtonsoft.Json;

namespace Feed.Lib.Converters {

    public class UserConverter : JsonConverter<User> {
        private readonly IDictionary<string, User>? _users;

        public UserConverter() {
            _users = null;
        }

        public UserConverter(IDictionary<string, User> users) {
            _users = users;
        }

        private User GetUser(string? name) {
            if (name == null) {
                return new User { Name = "Unknown" };
            }

            if (_users != null && _users.ContainsKey(name)) {
                return _users[name];
            } else {
                return new User { Name = name };
            }
        }

        public override User ReadJson(JsonReader reader, Type objectType, User? existingValue, bool hasExistingValue, JsonSerializer serializer) {
            return GetUser(reader.ReadAsString());
        }

        public override void WriteJson(JsonWriter writer, User? value, JsonSerializer serializer) {
            if (value != null) {
                writer.WriteValue(value.Name);
            }
        }
    }
}