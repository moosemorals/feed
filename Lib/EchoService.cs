using System;
using System.Threading.Tasks;

using Feed.Lib.Network;
using Feed.Models;

namespace Feed.Lib {

    public class EchoService : IPlugin {
        public Action<Command>? CommandSender { get; set; }

        public string Name => "Echo";

        public bool HasJavascript => false;

        public bool Match(Command cmd) => true;

        public void ExecuteCommand(Command cmd, Action<Command> SendReply) => SendReply(cmd);
    }
}
