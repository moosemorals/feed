﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Feed.Models;

namespace Feed.Lib {
    public interface IPlugin {

        public string Name { get; }

        public bool HasJavascript { get; }

        /// <summary>
        /// Find out if this plugin is intrested in this command before actually runinng it
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public bool Match(Command cmd);

        /// <summary>
        /// Do the work needed to process this command
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="SendReply"></param>
        public void ExecuteCommand(Command cmd, Action<Command> SendReply);
    }

    public interface IPluginAsync : IPlugin {

        public Task ExecuteCommandAsync(Command cmd, Action<Command> SendReply);
    }


}
