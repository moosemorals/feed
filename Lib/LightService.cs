﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Feed.Lib.Network;
using Feed.Models;

using Lights.Models;
using Lights.Services;

using Serilog;

namespace Feed.Lib {
    public class LightService : IPluginAsync {

        private readonly ILogger _log = Log.ForContext<LightService>();
        private readonly HueService _hue;

        private static int idCounter = 0;

        public LightService(HueService hue) {
            _hue = hue;
        }

        public string Name => "Lights";

        public bool HasJavascript => false;

        public bool Match(Command cmd) => cmd.To != null && cmd.To == Name;

        public async Task ExecuteCommandAsync(Command cmd, Action<Command> SendReply) {

            Hub? hub = await _hue.GetHub().ConfigureAwait(false);

            if (hub == null) {
                SendReply(BuildMessage(cmd.From, "Please click the button on the top of the hue hub"));

                ApiResponse<Hub> response = await _hue.FindHub().ConfigureAwait(false);

                if (response.IsSuccess) {
                    SendReply(BuildMessage(cmd.From, "Connected to the hue hub, thanks"));
                    hub = response.Payload;
                } else {

                    SendReply(BuildMessage(cmd.From, $"There was a problem connecting to the hub: {response.Error?.Description}"));
                    return;
                }
            }

            if (hub == null) {
                // shouldn't get here
                _log.Warning("Hub null when it shouldn't be");
                return;
            }

            string[] words = cmd.Payload.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            words = words[1..]; // Dump first word (will be 'lights');

            switch (words[0]) {
            case "list":
                foreach (Light l in await _hue.GetLightsAsync(hub)) {
                    SendReply(BuildMessage(cmd.From, $"Found light: {l.Name}"));
                }
                break;
            default:
                SendReply(BuildMessage(cmd.From, $"Unknown command"));
                break;
            }
        }

        private static Command BuildMessage(string To, string payload) {

            DateTimeOffset now = DateTimeOffset.Now;

            return new Command(
                now,
                $"l{now.ToUnixTimeMilliseconds()}-{idCounter++}",
                "print",
                "Lights",
                To,
                payload
            );
        }

        public void ExecuteCommand(Command cmd, Action<Command> SendReply) => throw new NotImplementedException();
    }
}
