using System.Collections.Generic;

namespace Feed.Lib {

    public class Map<TKey, TValue> where TKey : notnull {
        private readonly Dictionary<TKey, TValue?> _store = new();

        public TValue? this[TKey key] {
            get {
                if (_store.ContainsKey(key)) {
                    return _store[key];
                }
                return default;
            }
            set {
                if (_store.ContainsKey(key)) {
                    _store[key] = value;
                } else {
                    _store.Add(key, value);
                }
            }
        }

        public bool Has(TKey key) => _store.ContainsKey(key);
    }
}