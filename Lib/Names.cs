namespace Feed.Lib {

    public static class Names {

        public const long CommandVersion = 3;
        
        public static class Routes {
            public const string TopIndex = "route.top.index";
            public const string FeedIndex = "route.feed.index";
        }
    }
}