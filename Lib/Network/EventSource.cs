﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Feed.Lib.Network;
using Feed.Models;

using Serilog;

namespace Feed.Lib.Network {
    public class EventSource : IDisposable {

        private readonly ILogger _log = Log.ForContext<EventSource>();
        private readonly IEnumerable<IPlugin> _plugins ;
        private readonly BlockingCollection<Command> _sendQueue = new();
        private readonly Task _background;

        private bool disposedValue;

        public EventHandler<Command>? OutgoingEvent;

        public EventSource(IEnumerable<IPlugin> plugins) {
            _background = Task.Run(Background);
            _plugins = plugins;
        }

        private void Background() {
            while (!_sendQueue.IsCompleted) {
                Command cmd = _sendQueue.Take();
                Delegate[]? delegates = OutgoingEvent?.GetInvocationList();
                if (delegates == null) {
                    continue;
                }
                foreach (Delegate d in delegates) {
                    try {
                        d.DynamicInvoke(this, cmd);
                    } catch (Exception ex) {
                        _log.Error(ex, "Problem calling callback");
                    }
                }
            }
        }

        public void SendMessage(Command command) => _sendQueue.Add(command);

        public void ReceiveMessage(Command command) {
            foreach (IPlugin p in _plugins.Where(p => p.Match(command))) {
                if (p is IPluginAsync pAsync) {
                    pAsync.ExecuteCommandAsync(command, SendMessage)
                        .GetAwaiter().GetResult();
                } else {
                    p.ExecuteCommand(command, SendMessage);
                }
            }
        }

        public static string FormatSSE(string payload) => $"data: {payload}\n\n";

        public static string Heartbeat => ":\n";

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    _sendQueue.CompleteAdding();
                    _background.Wait();
                    _background.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose() {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
