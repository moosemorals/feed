using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore.Infrastructure;

using Serilog;

namespace Feed.Lib.Script {
    internal class CSharpInterpreter : IInterpreter, Stmt.IVisitor<Expression>, Expr.IVisitor<Expression> {
        private readonly ILogger _log = Log.ForContext<CSharpInterpreter>();

        private readonly ScriptEngine _engine;

        public CSharpInterpreter(ScriptEngine engine) {
            _engine = engine;
        }

        public void Interpret(List<Stmt> statements) {
            Expression<Action> func = Expression.Lambda<Action>(
                Expression.Block(statements.Select(s => Evaluate(s)))
            );

            _log.Debug("About to compile and run {func}", func.Print());

            func.Compile()();
        }

        public Expression VisitAssign(Expr.Assign expr) => throw new NotImplementedException();
        public Expression VisitBinary(Expr.Binary expr) {
            Expression left = Evaluate(expr.Left);
            Expression right = Evaluate(expr.Right);

            switch (expr.Op.Type) {
            case TokenType.BANG_EQUAL:
                return Expression.Not(Expression.Equal(left, right));
            case TokenType.EQUAL_EQUAL:
                return Expression.Equal(left, right);

            case TokenType.PLUS: {
                return Expression.TypeAs(Expression.Add(left, right), typeof(object));
            }
            case TokenType.MINUS: {
                return Expression.TypeAs(Expression.Subtract(left, right), typeof(object));
            }
            case TokenType.SLASH: {
                return Expression.TypeAs(Expression.Divide(left, right), typeof(object));
            }
            case TokenType.STAR: {
                return Expression.TypeAs(Expression.Multiply(left, right), typeof(object));
            }
            default:
                throw new NotImplementedException();
            }
        }
        public Expression VisitBlock(Stmt.Block stmt) {
            return Expression.Block(stmt.Statements.Select(s => Evaluate(s)));
        }
        public Expression VisitCall(Expr.Call expr) {
            Expression callee = Evaluate(expr.Callee);
            MethodInfo mi = typeof(ILoxCallable).GetMethod(nameof(ILoxCallable.Call)) ?? throw new Exception("Missing method");
            return Expression.Call(Expression.TypeAs(callee, typeof(ILoxCallable)), mi, expr.Arguments.Select(a => Evaluate(a)));
        }

        public Expression VisitExpression(Stmt.Expression stmt) {
            return Evaluate(stmt.Expr);
        }

        public Expression VisitFunction(Stmt.Function stmt) => throw new NotImplementedException();

        public Expression VisitGrouping(Expr.Grouping expr) {
            return Evaluate(expr.Expr);
        }

        public Expression VisitIf(Stmt.If stmt) {
            if (stmt.ElseBranch != null) {
                return Expression.IfThenElse(Evaluate(stmt.Condition), Evaluate(stmt.ThenBranch), Evaluate(stmt.ElseBranch));
            } else {
                return Expression.IfThen(Evaluate(stmt.Condition), Evaluate(stmt.ThenBranch));
            }
        }

        public Expression VisitLiteral(Expr.Literal expr) => new Value(expr.Value).ToExpression;

        public Expression VisitLogical(Expr.Logical expr) => throw new NotImplementedException();

        public Expression VisitPrint(Stmt.Print stmt) {
            MethodInfo mi = typeof(ScriptEngine).GetMethod(nameof(ScriptEngine.Output)) ?? throw new Exception("Missing method");

            Expression e = Evaluate(stmt.Expr);

            return Expression.Call(Expression.Constant(_engine), mi, Stringify(e));
        }

        public Expression VisitReturn(Stmt.Return stmt) => throw new NotImplementedException();

        public Expression VisitUnary(Expr.Unary expr) => throw new NotImplementedException();

        public Expression VisitVar(Stmt.Var stmt) => throw new NotImplementedException();

        public Expression VisitVariable(Expr.Variable expr) => throw new NotImplementedException();

        public Expression VisitWhile(Stmt.While stmt) {
            LabelTarget label = Expression.Label();
            return Expression.Loop(
                Expression.IfThenElse(
                    Evaluate(stmt.Condition),
                    Evaluate(stmt.Body),
                    Expression.Break(label)
                ),
                label
            );
        }

        private Expression Evaluate(Expr expr) => expr.Accept(this);

        private Expression Evaluate(Stmt stmt) => stmt.Accept(this);

        private (Expression, Expression) CheckNumberOperands(Token op, Expression left, Expression right) {
            if (left.Type == typeof(double) && right.Type == typeof(double)) {
                return (left, right);
            }
            throw new InterpretException(op, "Both sides must be numbers");
        }

        private static Expression Stringify(Expression expr) {
            MethodInfo mi = typeof(CSharpInterpreter).GetMethod(nameof(StringifyBase)) ?? throw new Exception("Missing method");
            return Expression.Call(mi, expr);
        }

        public static string StringifyBase(object? value) {
            if (value == null) {
                return "nil";
            }
            if (value is double d) {
                return d.ToString();
            }
            return value.ToString() ?? "";
        }
    }


    internal class Value {

        private readonly object? _value;

        public Value(object? value) {
            _value = value;
        }

        public Expression AsDouble => Expression.TypeAs(Expression.Constant(_value), typeof(Double));

        public Expression AsString => Expression.TypeAs(Expression.Constant(_value), typeof(string));

        public Expression AsNil => Expression.Constant(null);

        public Expression ToExpression => _value switch {
            double => AsDouble,
            string => AsString,
            _ => AsNil
        };


    }
}

