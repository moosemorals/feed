using System;

namespace Feed.Lib.Script {

    internal class Environment {
        private readonly Map<string, LoxValue> _values = new();

        private readonly Environment? _enclosing;

        internal Environment() {
            _enclosing = null;
        }

        internal Environment(Environment enclosing) {
            _enclosing = enclosing;
        }

        internal void Assign(Token name, LoxValue value) {
            if (_values.Has(name.Lexeme)) {
                _values[name.Lexeme] = value;
                return;
            }

            if (_enclosing != null) {
                _enclosing.Assign(name, value);
                return;
            }

            throw new InterpretException(name, $"Undefined variable '{name.Lexeme}'.");
        }

        internal void Define(string name, LoxValue value) => _values[name] = value;

        internal LoxValue Get(Token name) {
            if (_values.Has(name.Lexeme)) {
                return _values[name.Lexeme]!;
            }

            if (_enclosing != null) {
                return _enclosing.Get(name);
            }

            throw new InterpretException(name, $"Undefined variable '{name.Lexeme}'.");
        }

        internal LoxValue GetAt(int dist, string name)
            => Ancestor(dist)._values[name]!;

        internal void AssignAt(int dist, Token name, LoxValue value)
            => Ancestor(dist)._values[name.Lexeme] = value;

        internal Environment Ancestor(int dist) {
            Environment? env = this;
            for (int i = 0; i < dist; i += 1) {
                env = env?._enclosing;
            }
            if (env == null) {
                throw new Exception("Intenral error: Env doesn't have a parent");
            }
            return env;
        }
    }
}