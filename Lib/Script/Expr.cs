using System.Collections.Generic;

namespace Feed.Lib.Script {
    internal abstract record Expr {
        internal record Assign(Token Name, Expr Value) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitAssign(this);
        }

        internal record Binary(Expr Left, Token Op, Expr Right) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitBinary(this);
        }
        internal record Call(Expr Callee, Token Paren, List<Expr> Arguments) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitCall(this);
        }

        internal record Grouping(Expr Expr) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitGrouping(this);
        }

        internal record Literal(LoxValue Value) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitLiteral(this);
        }

        internal record Logical(Expr Left, Token Op, Expr Right) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitLogical(this);
        }

        internal record Unary(Token Op, Expr Right) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitUnary(this);
        }

        internal record Variable(Token Name) : Expr {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitVariable(this);
        }

        internal interface IVisitor<T> {
            T VisitAssign(Assign expr);
            T VisitBinary(Binary expr);
            T VisitCall(Call expr);
            T VisitGrouping(Grouping expr);
            T VisitLiteral(Literal expr);
            T VisitLogical(Logical expr);
            T VisitUnary(Unary expr);
            T VisitVariable(Variable expr);
        }

        internal abstract T Accept<T>(IVisitor<T> visitor);
    }
}