using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feed.Lib.Script {
    interface IInterpreter {

        public void Interpret(List<Stmt> statements);

    }
}
