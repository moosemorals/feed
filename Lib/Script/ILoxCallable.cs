using System.Collections.Generic;

namespace Feed.Lib.Script {

    internal interface ILoxCallable {

        LoxValue Call(LoxInterpreter interpreter, List<LoxValue> args);

        int Arity { get; }
    }
}