using System;

namespace Feed.Lib.Script {

    internal class InterpretException : Exception {
        internal Token Token { get; init; }

        internal InterpretException(Token token, string message) : base(message) {
            Token = token;
        }
    }
}