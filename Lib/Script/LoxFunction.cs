using System.Collections.Generic;
using System.Linq;

namespace Feed.Lib.Script {

    internal class LoxFunction : ILoxCallable {
        private readonly Stmt.Function _decl;
        private readonly Environment _closure;

        internal LoxFunction(Stmt.Function decl, Environment closure) {
            _closure = closure;
            _decl = decl;
        }

        public int Arity => _decl.Params.Count;

        public LoxValue Call(LoxInterpreter interpreter, List<LoxValue> args) {
            Environment env = new Environment(_closure);

            for (int i = 0; i < _decl.Params.Count; i += 1) {
                env.Define(_decl.Params[i].Lexeme, args[i]);
            }

            try {
                interpreter.ExecuteBlock(_decl.Body, env);
            } catch (Return r) {
                return r.Value;
            }

            return LoxValue.Nil;
        }

        public override string ToString()
            => $"<fn {_decl.Name.Lexeme}({string.Join(", ", _decl.Params.Select(t => t.Lexeme))})>";
    }
}