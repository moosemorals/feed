using System;
using System.Collections.Generic;

using static Feed.Lib.Script.TokenType;

namespace Feed.Lib.Script {

    internal class LoxInterpreter : IInterpreter, Stmt.IVisitor<object?>, Expr.IVisitor<LoxValue> {
        private readonly Map<Expr, int> _locals = new();
        private Environment _environment;

        internal ScriptEngine Engine { get; init; }
        internal Environment Globals { get; init; }

        public LoxInterpreter(ScriptEngine engine) {
            Globals = new();
            Globals.Define("clock", new LoxValue(new NativeClock()));

            _environment = Globals;
            Engine = engine;
        }

        public void Interpret(List<Stmt> statements) {
            try {
                foreach (Stmt s in statements) {
                    Execute(s);
                }
            } catch (InterpretException ex) {
                Engine.Error(ex);
            }
        }

        #region Statements

        public object? VisitBlock(Stmt.Block stmt) {
            ExecuteBlock(stmt.Statements, new Environment(_environment));
            return null;
        }

        public object? VisitExpression(Stmt.Expression stmt) {
            Evaluate(stmt.Expr);
            return null;
        }

        public object? VisitFunction(Stmt.Function stmt) {
            LoxValue func = new LoxValue(new LoxFunction(stmt, _environment));
            _environment.Define(stmt.Name.Lexeme, func);
            return null;
        }

        public object? VisitIf(Stmt.If stmt) {
            if (IsTruthy(Evaluate(stmt.Condition))) {
                Execute(stmt.ThenBranch);
            } else if (stmt.ElseBranch != null) {
                Execute(stmt.ElseBranch);
            }
            return null;
        }

        public object? VisitPrint(Stmt.Print stmt) {
            LoxValue value = Evaluate(stmt.Expr);
            Engine.Output(Stringify(value));
            return null;
        }

        public object? VisitReturn(Stmt.Return stmt) {
            LoxValue value = LoxValue.Nil;
            if (stmt.Value != null) {
                value = Evaluate(stmt.Value);
            }

            throw new Return(value);
        }

        public object? VisitVar(Stmt.Var stmt) {
            LoxValue value = LoxValue.Nil;
            if (stmt.Initializer != null) {
                value = Evaluate(stmt.Initializer);
            }

            _environment.Define(stmt.Name.Lexeme, value);

            return null;
        }

        public object? VisitWhile(Stmt.While stmt) {
            while (IsTruthy(Evaluate(stmt.Condition))) {
                Execute(stmt.Body);
            }
            return null;
        }

        #endregion Statement

        #region Expression

        public LoxValue VisitAssign(Expr.Assign expr) {
            LoxValue value = Evaluate(expr.Value);

            int? dist = _locals[expr];
            if (dist != null) {
                _environment.AssignAt((int)dist, expr.Name, value);
            } else {
                Globals.Assign(expr.Name, value);
            }

            return value;
        }

        public LoxValue VisitBinary(Expr.Binary expr) {
            LoxValue left = Evaluate(expr.Left);
            LoxValue right = Evaluate(expr.Right);

            switch (expr.Op.Type) {
            case BANG_EQUAL:
                return new LoxValue(!IsEqual(left, right));

            case EQUAL_EQUAL:
                return new LoxValue(IsEqual(left, right));

            case GREATER: {
                CheckNumberOperands(expr.Op, left, right);
                return new LoxValue(left.AsNumber > right.AsNumber);
            }
            case GREATER_EQUAL: {
                CheckNumberOperands(expr.Op, left, right);
                return new LoxValue(left.AsNumber >= right.AsNumber);
            }
            case LESS: {
                CheckNumberOperands(expr.Op, left, right);
                return new LoxValue(left.AsNumber < right.AsNumber);
            }
            case LESS_EQUAL: {
                CheckNumberOperands(expr.Op, left, right);
                return new LoxValue(left.AsNumber <= right.AsNumber);
            }
            case MINUS: {
                CheckNumberOperands(expr.Op, left, right);
                return new LoxValue(left.AsNumber - right.AsNumber);
            }
            case SLASH: {
                CheckNumberOperands(expr.Op, left, right);
                return new LoxValue(left.AsNumber / right.AsNumber);
            }
            case STAR: {
                CheckNumberOperands(expr.Op, left, right);
                return new LoxValue(left.AsNumber * right.AsNumber);
            }
            case PLUS:
                if (left.IsNumber && right.IsNumber) {
                    return new LoxValue(left.AsNumber + right.AsNumber);
                } else if (left.IsString && right.IsString) {
                    return new LoxValue(left.AsString + right.AsString);
                }

                throw new InterpretException(expr.Op, "Operands must be two numbers or two strings");
            }

            throw new Exception("Unreachable code reached");
        }

        public LoxValue VisitCall(Expr.Call expr) {
            LoxValue callee = Evaluate(expr.Callee);

            List<LoxValue> arguments = new();
            foreach (Expr arg in expr.Arguments) {
                arguments.Add(Evaluate(arg));
            }

            if (callee.IsFunction) {
                if (arguments.Count == callee.AsFunction.Arity) {
                    return callee.AsFunction.Call(this, arguments);
                }
                throw new InterpretException(expr.Paren, $"Expected {callee.AsFunction.Arity} argument(s) but got {arguments.Count} instead.");
            }

            throw new InterpretException(expr.Paren, "Can only call functions and classes.");
        }

        public LoxValue VisitGrouping(Expr.Grouping expr) => Evaluate(expr.Expr);

        public LoxValue VisitLiteral(Expr.Literal expr) => expr.Value;

        public LoxValue VisitLogical(Expr.Logical expr) {
            LoxValue left = Evaluate(expr.Left);

            if (expr.Op.Type == OR) {
                if (IsTruthy(left)) {
                    return left;
                }
            } else {
                if (!IsTruthy(left)) {
                    return left;
                }
            }

            return Evaluate(expr.Right);
        }

        public LoxValue VisitUnary(Expr.Unary expr) {
            LoxValue right = Evaluate(expr.Right);

            switch (expr.Op.Type) {
            case MINUS:
                CheckNumberOperand(expr.Op, right);
                return new LoxValue(-right.AsNumber);

            case BANG:
                return new LoxValue(!IsTruthy(right));
            }

            throw new Exception("Unreachable code reached");
        }

        public LoxValue VisitVariable(Expr.Variable expr) => LookupVariable(expr.Name, expr);

        #endregion Expression

        #region Tools

        internal void Resolve(Expr expr, int depth) => _locals[expr] = depth;

        private LoxValue Evaluate(Expr expr) => expr.Accept(this);

        private void Execute(Stmt stmt) => stmt.Accept(this);

        internal void ExecuteBlock(List<Stmt> statements, Environment environmen) {
            Environment previous = _environment;
            try {
                _environment = environmen;
                foreach (Stmt s in statements) {
                    Execute(s);
                }
            } finally {
                _environment = previous;
            }
        }

        private LoxValue LookupVariable(Token name, Expr expr) {
            int? dist = _locals[expr];
            if (dist != null) {
                return _environment.GetAt((int)dist, name.Lexeme);
            } else {
                return Globals.Get(name);
            }
        }

        private static void CheckNumberOperands(Token op, LoxValue left, LoxValue right) {
            if (!left.IsNumber || !right.IsNumber) {
                throw new InterpretException(op, "Operands must be numbers");
            }
        }

        private static void CheckNumberOperand(Token op, LoxValue right) {
            if (!right.IsNumber) {

                throw new InterpretException(op, "Operand must be a number");
            }
        }

        private static bool IsEqual(LoxValue a, LoxValue b) {
            if (a.IsNil && b.IsNil) {
                return true;
            }
            if (a.IsNil) {
                return false;
            }

            return a.Equals(b);
        }

        private static bool IsTruthy(LoxValue value) {
            if (value.IsNil) {
                return false;
            }
            if (value.IsBool) {
                return value.AsBool;
            }
            return true;
        }

        private static string Stringify(LoxValue value) => value.ToString() ?? "nil";

        #endregion Tools
    }
}