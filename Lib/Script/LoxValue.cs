﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feed.Lib.Script {
    internal class LoxValue {

        private readonly object _value;
        private readonly LoxType _type;

        private LoxValue(LoxType type, object value) {
            _type = type;
            _value = value;
        }

        internal static LoxValue Nil => new(LoxType.Nil, new object());
        internal static LoxValue True => new(LoxType.Bool, true);
        internal static LoxValue False => new(LoxType.Bool, false);


        internal LoxValue(double value) : this(LoxType.Number, value) { }
        internal bool IsNumber => _type == LoxType.Number;
        internal double AsNumber => IsNumber ? (double)_value : throw new ArgumentException("Value is not a number");

        internal LoxValue(ILoxCallable value) : this(LoxType.Function, value) { }
        internal bool IsFunction => _type == LoxType.Function;
        internal ILoxCallable AsFunction => IsFunction ? (ILoxCallable)_value : throw new ArgumentException("Value is not a function");

        internal LoxValue(string value) : this(LoxType.String, value) { }
        internal bool IsString => _type == LoxType.String;
        internal string AsString => IsString ? (string)_value : throw new ArgumentException("Value is not a string");

        internal LoxValue(bool value) : this(LoxType.Bool, value) { }
        internal bool IsBool => _type == LoxType.Bool;
        internal bool AsBool => IsBool ? (bool)_value : throw new ArgumentException("Value is not a bool");

        internal bool IsNil => _type == LoxType.Nil;
        internal object? AsNil => IsNil ? Nil : throw new ArgumentException("Value is not nil");

        public override string? ToString() => IsNil ? "nil" : _value.ToString();

        private enum LoxType {
            Array,
            Bool,
            Function,
            Nil,
            Number,
            Object,
            String,
        }


    }
}
