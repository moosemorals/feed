using System;
using System.Collections.Generic;

namespace Feed.Lib.Script {

    internal class NativeClock : ILoxCallable {
        public int Arity => 0;

        public LoxValue Call(LoxInterpreter interpreter, List<LoxValue> args)
            => new LoxValue(DateTimeOffset.Now.ToUnixTimeSeconds());

        public override string ToString() => "<native fn clock()>";
    }
}