using System;
using System.Collections.Generic;

using static Feed.Lib.Script.TokenType;

namespace Feed.Lib.Script {

    internal class Parser {

        private class ParseException : Exception { }

        private readonly List<Token> _tokens;
        private readonly ScriptEngine _engine;
        private int _current = 0;

        internal Parser(ScriptEngine engine, List<Token> tokens) {
            _engine = engine;
            _tokens = tokens;
        }

        internal List<Stmt> Parse() {
            List<Stmt> statements = new();

            while (!IsAtEnd) {
                Stmt? stmt = Declaration();
                if (stmt != null) {
                    statements.Add(stmt);
                }
            }

            return statements;
        }

        #region Statements

        private Stmt? Declaration() {
            try {
                if (Match(FUNC)) {
                    return Function("function");
                }
                if (Match(VAR)) {
                    return VarDeclaration();
                }
                return Statement();
            } catch (ParseException) {
                Synchronize();
                return null;
            }
        }

        private Stmt Statement() {
            if (Match(FOR)) {
                return ForStatement();
            }
            if (Match(IF)) {
                return IfStatement();
            }
            if (Match(PRINT)) {
                return PrintStatement();
            }
            if (Match(RETURN)) {
                return ReturnStatement();
            }
            if (Match(WHILE)) {
                return WhileStatement();
            }
            if (Match(LEFT_BRACE)) {
                return new Stmt.Block(Block());
            }
            return ExpressionStatement();
        }

        private List<Stmt> Block() {
            List<Stmt> statements = new();

            while (!IsAtEnd && !Check(RIGHT_BRACE)) {
                Stmt? stmt = Declaration();
                if (stmt != null) {
                    statements.Add(stmt);
                }
            }

            Consume(RIGHT_BRACE, "Expected '}' after block.");
            return statements;
        }

        private Stmt ForStatement() {
            Consume(LEFT_PAREN, "Expected '(' after 'for'.");

            Stmt? initializer;
            if (Match(SEMICOLON)) {
                initializer = null;
            } else if (Match(VAR)) {
                initializer = VarDeclaration();
            } else {
                initializer = ExpressionStatement();
            }

            Expr condition = new Expr.Literal(LoxValue.True);
            if (!Check(SEMICOLON)) {
                condition = Expression();
            }
            Consume(SEMICOLON, "Expected ';' after for condition.");

            Expr? increment = null;
            if (!Check(SEMICOLON)) {
                increment = Expression();
            }
            Consume(RIGHT_PAREN, "Expected ';' after for clauses.");

            Stmt body = Statement();

            if (increment != null) {
                body = new Stmt.Block(new List<Stmt> {
                    body,
                    new Stmt.Expression(increment),
                });
            }

            body = new Stmt.While(condition, body);

            if (initializer != null) {
                body = new Stmt.Block(new List<Stmt> {
                    initializer,
                    body,
                });
            }

            return body;
        }

        private Stmt Function(string kind) {
            Token name = Consume(IDENTIFIER, $"Expected {kind} name.");

            Consume(LEFT_PAREN, $"Expected '(' after {kind} name.");
            List<Token> parameters = new();
            if (!Check(RIGHT_PAREN)) {
                do {
                    if (parameters.Count >= 255) {
                        Error(Peek(), "Can't have more than 255 parameters.");
                    }

                    parameters.Add(Consume(IDENTIFIER, "Expected parameter name."));
                } while (Match(COMMA));
            }
            Consume(RIGHT_PAREN, "Expected ')' after parameters.");

            Consume(LEFT_BRACE, $"Expected '{{' before {kind} body.");

            List<Stmt> body = Block();
            return new Stmt.Function(name, parameters, body);
        }

        private Stmt IfStatement() {
            Consume(LEFT_PAREN, "Expected '(' after 'if'.");
            Expr condition = Expression();
            Consume(RIGHT_PAREN, "Expected ')' after if condition.");

            Stmt thenBranch = Statement();
            Stmt? elseBranch = null;
            if (Match(ELSE)) {
                elseBranch = Statement();
            }

            return new Stmt.If(condition, thenBranch, elseBranch);
        }

        private Stmt ExpressionStatement() {
            Expr value = Expression();
            Consume(SEMICOLON, "Expected ';' after expression.");
            return new Stmt.Expression(value);
        }

        private Stmt PrintStatement() {
            Expr value = Expression();
            Consume(SEMICOLON, "Expected ';' after value.");
            return new Stmt.Print(value);
        }

        private Stmt ReturnStatement() {
            Token keyword = Previous();
            Expr? value = null;
            if (!Check(SEMICOLON)) {
                value = Expression();
            }

            Consume(SEMICOLON, "Expected ';' after return value");
            return new Stmt.Return(keyword, value);
        }

        private Stmt VarDeclaration() {
            Token name = Consume(IDENTIFIER, "Expected variable name.");

            Expr? initializer = null;
            if (Match(EQUAL)) {
                initializer = Expression();
            }

            Consume(SEMICOLON, "Expected ';' after variable declration.");
            return new Stmt.Var(name, initializer);
        }

        private Stmt WhileStatement() {
            Consume(LEFT_PAREN, "Expect '(' after 'while'.");
            Expr condition = Expression();
            Consume(RIGHT_PAREN, "Expect ')' after while condition.");
            Stmt body = Statement();

            return new Stmt.While(condition, body);
        }

        #endregion Statements

        #region Expressions

        private Expr ParseBinary(Func<Expr> next, params TokenType[] types) {
            Expr expr = next();
            while (Match(types)) {
                Token op = Previous();
                Expr right = next();
                expr = new Expr.Binary(expr, op, right);
            }
            return expr;
        }
        private Expr ParseLogical(Func<Expr> next, params TokenType[] types) {
            Expr expr = next();
            while (Match(types)) {
                Token op = Previous();
                Expr right = next();
                expr = new Expr.Logical(expr, op, right);
            }
            return expr;
        }


        private Expr Primary() {
            if (Match(FALSE)) {
                return new Expr.Literal(LoxValue.False);
            }
            if (Match(TRUE)) {
                return new Expr.Literal(LoxValue.True);
            }
            if (Match(NIL)) {
                return new Expr.Literal(LoxValue.Nil);
            }

            if (Match(NUMBER)) {
                if (Previous().Literal is double d) {
                    return new Expr.Literal(new LoxValue(d));
                } else {
                    throw Error(Previous(), "Looks like a number, but isn't");
                }
            }

            if (Match(STRING)) {
                if (Previous().Literal is string s) {
                    return new Expr.Literal(new LoxValue(s));
                } else {
                    throw Error(Previous(), "Looks like a string, but isn't");
                }
            }

            if (Match(IDENTIFIER)) {
                return new Expr.Variable(Previous());
            }

            if (Match(LEFT_PAREN)) {
                Expr expr = Expression();
                Consume(RIGHT_PAREN, "Expected ')' after expression.");
                return new Expr.Grouping(expr);
            }

            throw Error(Peek(), "Expected expression.");
        }

        private Expr Call() {
            Expr expr = Primary();

            while (true) {
                if (Match(LEFT_PAREN)) {
                    expr = FinishCall(expr);
                } else {
                    break;
                }
            }
            return expr;
        }

        private Expr FinishCall(Expr callee) {
            List<Expr> arguments = new();

            if (!Check(RIGHT_PAREN)) {
                do {
                    if (arguments.Count >= 255) {
                        Error(Peek(), "Can't have more than 255 arguments");
                    }
                    arguments.Add(Expression());
                } while (Match(COMMA));
            }

            Token paren = Consume(RIGHT_PAREN, "Expected ')' after arguments.");

            return new Expr.Call(callee, paren, arguments);
        }

        private Expr Unary() {
            if (Match(BANG, MINUS)) {
                Token op = Previous();
                Expr right = Unary();
                return new Expr.Unary(op, right);
            }
            return Call();
        }

        private Func<Expr> Factor => () => ParseBinary(Unary, SLASH, STAR);
        private Func<Expr> Term => () => ParseBinary(Factor, MINUS, PLUS);
        private Func<Expr> Comparison => () => ParseBinary(Term, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL);
        private Func<Expr> Equality => () => ParseBinary(Comparison, BANG_EQUAL, EQUAL_EQUAL);
        private Func<Expr> And => () => ParseLogical(Equality, AND);
        private Func<Expr> Or => () => ParseLogical(And, OR);

        private Expr Assignment() {
            Expr expr = Or();

            if (Match(EQUAL)) {
                Token equals = Previous();
                Expr value = Assignment();

                if (expr is Expr.Variable variable) {
                    Token name = variable.Name;
                    return new Expr.Assign(name, value);
                }

                Error(equals, "Invalid assignment target.");
            }

            return expr;
        }

        private Expr Expression() {
            return Assignment();
        }

        #endregion Expressions

        #region Tools

        private Token Advance() {
            if (!IsAtEnd) {
                _current += 1;
            }

            return Previous();
        }

        private bool Check(TokenType type) => !IsAtEnd && Peek().Type == type;

        private Token Consume(TokenType type, string message) {
            if (Check(type)) {
                return Advance();
            }

            throw Error(Peek(), message);
        }

        private ParseException Error(Token token, string message) {
            _engine.Error(token, message);
            return new ParseException();
        }

        private bool IsAtEnd => Peek().Type == EOF;

        private bool Match(params TokenType[] types) {
            foreach (TokenType type in types) {
                if (Check(type)) {
                    Advance();
                    return true;
                }
            }
            return false;
        }

        private Token Peek() => _tokens[_current];

        private Token Previous() => _tokens[_current - 1];

        private void Synchronize() {
            Advance();

            while (!IsAtEnd) {
                if (Previous().Type == SEMICOLON) {
                    return;
                }

                switch (Peek().Type) {
                case CLASS:
                case FUNC:
                case VAR:
                case FOR:
                case IF:
                case WHILE:
                case PRINT:
                case RETURN:
                    return;
                }
                Advance();
            }
        }

        #endregion Tools
    }
}