using System;

namespace Feed.Lib.Script {

    public class Return : Exception {
        internal LoxValue Value { get; init; }

        internal Return(LoxValue value) {
            Value = value;
        }
    }
}