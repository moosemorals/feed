using System.Collections.Generic;
using System.Linq;

namespace Feed.Lib.Script {

    internal class Resolver : Expr.IVisitor<object?>, Stmt.IVisitor<object?> {
        private readonly LoxInterpreter _interpreter;
        private readonly Stack<Dictionary<string, bool>> _scopes = new();

        private FunctionType _currentFunction = FunctionType.None;

        private enum FunctionType {
            None,
            Function,
        }

        internal Resolver(LoxInterpreter interpreter) {
            _interpreter = interpreter;
        }

        #region Statements

        public object? VisitBlock(Stmt.Block stmt) {
            BeginScope();
            Resolve(stmt.Statements);
            EndScope();
            return null;
        }

        public object? VisitExpression(Stmt.Expression stmt) {
            Resolve(stmt.Expr);
            return null;
        }

        public object? VisitIf(Stmt.If stmt) {
            Resolve(stmt.Condition);
            Resolve(stmt.ThenBranch);
            if (stmt.ElseBranch != null) {
                Resolve(stmt.ElseBranch);
            }
            return null;
        }

        public object? VisitFunction(Stmt.Function stmt) {
            Declare(stmt.Name);
            Define(stmt.Name);

            ResolveFunction(stmt, FunctionType.Function);

            return null;
        }

        public object? VisitPrint(Stmt.Print stmt) {
            Resolve(stmt.Expr);
            return null;
        }

        public object? VisitReturn(Stmt.Return stmt) {
            if (_currentFunction == FunctionType.None) {
                _interpreter.Engine.Error(stmt.Keyword, "Can't return from top-level code.");
            }
            if (stmt.Value != null) {
                Resolve(stmt.Value);
            }
            return null;
        }

        public object? VisitVar(Stmt.Var stmt) {
            Declare(stmt.Name);
            if (stmt.Initializer != null) {
                Resolve(stmt.Initializer);
            }
            Define(stmt.Name);
            return null;
        }

        public object? VisitWhile(Stmt.While stmt) {
            Resolve(stmt.Condition);
            Resolve(stmt.Body);
            return null;
        }

        #endregion Statements

        #region Expressions

        public object? VisitAssign(Expr.Assign expr) {
            Resolve(expr.Value);
            ResolveLocal(expr, expr.Name);
            return null;
        }

        public object? VisitBinary(Expr.Binary expr) {
            Resolve(expr.Left);
            Resolve(expr.Right);
            return null;
        }

        public object? VisitCall(Expr.Call expr) {
            Resolve(expr.Callee);

            foreach (Expr arg in expr.Arguments) {
                Resolve(arg);
            }

            return null;
        }

        public object? VisitGrouping(Expr.Grouping expr) {
            Resolve(expr.Expr);
            return null;
        }

        public object? VisitLiteral(Expr.Literal expr) => null;

        public object? VisitLogical(Expr.Logical expr) {
            Resolve(expr.Left);
            Resolve(expr.Right);
            return null;
        }

        public object? VisitUnary(Expr.Unary expr) {
            Resolve(expr.Right);
            return null;
        }

        public object? VisitVariable(Expr.Variable expr) {
            if (_scopes.Count > 0) {
                Dictionary<string, bool> scope = _scopes.Peek();
                if (scope.ContainsKey(expr.Name.Lexeme) && scope[expr.Name.Lexeme] == false) {
                    _interpreter.Engine.Error(expr.Name, "Can't read local variable in it's own initializer.");
                }
            }
            ResolveLocal(expr, expr.Name);
            return null;
        }

        #endregion Expressions

        #region Tools

        private void Declare(Token name) {
            if (_scopes.Count == 0) {
                return;
            }

            Dictionary<string, bool> scope = _scopes.Peek();
            if (scope.ContainsKey(name.Lexeme)) {
                _interpreter.Engine.Error(name, "Already variable with this name in this scope.");
            }
            scope.Add(name.Lexeme, false);
        }

        private void Define(Token name) {
            if (_scopes.Count == 0) {
                return;
            }
            _scopes.Peek()[name.Lexeme] = true;
        }

        private void BeginScope() {
            _scopes.Push(new Dictionary<string, bool>());
        }

        private void EndScope() {
            _scopes.Pop();
        }

        internal void Resolve(List<Stmt> statements) {
            foreach (Stmt s in statements) {
                Resolve(s);
            }
        }

        private void Resolve(Stmt stmt) => stmt.Accept(this);

        private void Resolve(Expr expr) => expr.Accept(this);

        private void ResolveFunction(Stmt.Function function, FunctionType type) {
            FunctionType enclosingFunction = _currentFunction;

            _currentFunction = type;

            BeginScope();
            foreach (Token param in function.Params) {
                Declare(param);
                Define(param);
            }
            Resolve(function.Body);
            EndScope();

            _currentFunction = enclosingFunction;
        }

        private void ResolveLocal(Expr expr, Token name) {
            for (int i = _scopes.Count - 1; i >= 0; i -= 1) {
                if (_scopes.ElementAt(i).ContainsKey(name.Lexeme)) {
                    _interpreter.Resolve(expr, _scopes.Count - 1 - i);
                    return;
                }
            }
        }

        #endregion Tools
    }
}