using System.Collections.Generic;

using static Feed.Lib.Script.TokenType;

namespace Feed.Lib.Script {

    internal class Scanner {

        private static readonly IDictionary<string, TokenType> _keywords = new Dictionary<string, TokenType> {
            {"and", AND },
            {"class", CLASS },
            {"else", ELSE },
            {"false", FALSE },
            {"for", FOR },
            {"func", FUNC },
            {"if", IF },
            {"nil", NIL },
            {"or", OR },
            {"print", PRINT },
            {"return", RETURN },
            {"super", SUPER },
            {"this", THIS },
            {"true", TRUE },
            {"var", VAR },
            {"while", WHILE },
        };

        private readonly ScriptEngine _engine;
        private readonly string _source;
        private readonly List<Token> _tokens = new();
        private int _start = 0;
        private int _current = 0;
        private int _line = 1;

        internal Scanner(ScriptEngine engine, string source) {
            _engine = engine;
            _source = source;
        }

        internal List<Token> ScanTokens() {
            while (!IsAtEnd) {
                _start = _current;
                ScanToken();
            }

            _tokens.Add(new Token(EOF, "", null, _line));

            return _tokens;
        }

        private void ScanToken() {
            char c = Advance();
            switch (c) {
            case '(': AddToken(LEFT_PAREN); break;
            case ')': AddToken(RIGHT_PAREN); break;
            case '{': AddToken(LEFT_BRACE); break;
            case '}': AddToken(RIGHT_BRACE); break;
            case ',': AddToken(COMMA); break;
            case '.': AddToken(DOT); break;
            case '-': AddToken(MINUS); break;
            case '+': AddToken(PLUS); break;
            case ';': AddToken(SEMICOLON); break;
            case '*': AddToken(STAR); break;
            case '!':
                AddToken(Match('=') ? BANG_EQUAL : BANG);
                break; 
            case '=':
                AddToken(Match('=') ? EQUAL_EQUAL : EQUAL);
                break; 
            case '<':
                AddToken(Match('=') ? LESS_EQUAL : LESS);
                break; 
            case '>':
                AddToken(Match('=') ? GREATER_EQUAL : GREATER);
                break; 
            case '/':
                if (Match('/')) {
                    // Comment to EOL
                    while (!IsAtEnd && Peek() != '\n') {
                        Advance();
                    }
                } else {
                    AddToken(SLASH);
                }
                break; 
            case ' ':
            case '\r':
            case '\t':
                // Ignorable whitespace
                break;

            case '\n':
                _line += 1;
                break;

            case '"': String(); break;

            default:
                if (IsDigit(c)) {
                    Number();
                } else if (IsAlpha(c)) {
                    Identifier();
                } else {
                    _engine.Error(_line, "Unexpected character.");
                }
                break;
            }
        }

        private char Advance() => _source[_current++];

        private char Peek() => IsAtEnd ? '\0' : _source[_current];

        private char PeekNext() {
            if (_current + 1 >= _source.Length) {
                return '\0';
            }

            return _source[_current + 1];
        }

        private bool IsAtEnd => _current >= _source.Length;

        private bool IsAlpha(char c) =>
            (c >= 'a' && c <= 'z')
            || (c >= 'A' && c <= 'Z')
            || c == '_';

        private bool IsAlphaNumeric(char c) => IsAlpha(c) || IsDigit(c);

        private bool IsDigit(char c) => c >= '0' && c <= '9';

        private bool Match(char expected) {
            if (IsAtEnd || _source[_current] != expected) {
                return false;
            }
            _current += 1;
            return true;
        }

        private void AddToken(TokenType type) => AddToken(type, null);

        private void AddToken(TokenType type, object? literal) {
            string lexeme = _source.Substring(_start, _current - _start);
            _tokens.Add(new Token(type, lexeme, literal, _line));
        }

        private void Identifier() {
            while (IsAlphaNumeric(Peek())) {
                Advance();
            }

            string text = _source.Substring(_start, _current - _start);
            if (_keywords.ContainsKey(text)) {
                AddToken(_keywords[text]);
            } else {
                AddToken(IDENTIFIER);
            }
        }

        private void Number() {
            while (IsDigit(Peek())) {
                Advance();
            }

            if (Peek() == '.' && IsDigit(PeekNext())) {
                // Read the '.'
                Advance();
                while (IsDigit(Peek())) {
                    Advance();
                }
            }

            string text = _source.Substring(_start, _current - _start);
            if (double.TryParse(text, out double value)) {
                AddToken(NUMBER, value);
            } else {
                _engine.Error(_line, "Can't parse number");
            }
        }

        private void String() {
            while (!IsAtEnd && Peek() != '"') {
                if (Peek() == '\n') {
                    _line += 1;
                }
                Advance();
            }

            if (IsAtEnd) {
                _engine.Error(_line, "Unterminated string literal");
                return;
            }

            // Consume closing quote
            Advance();

            string text = _source.Substring(_start + 1, _current - _start - 2);
            AddToken(STRING, text);
        }
    }
}