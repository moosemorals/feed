using System;
using System.Collections.Generic;

namespace Feed.Lib.Script {

    public class ScriptEngine {
        private ScriptOutput? _output;

        private readonly IInterpreter _interpreter;

        public ScriptEngine() {
            _interpreter = new LoxInterpreter(this);
        }

        public ScriptOutput Run(string source) {
            _output = new ScriptOutput();

            Scanner scanner = new(this, source);
            List<Token> tokens = scanner.ScanTokens();

            Parser parser = new(this, tokens);
            List<Stmt> statements = parser.Parse();

            if (HadError) {
                return _output;
            }

       //     new Resolver(_interpreter).Resolve(statements);
            if (HadError) {
                return _output;
            }

            _interpreter.Interpret(statements);

            return _output;
        }

        internal void Error(int line, string message) => Report(line, "", message);

        internal void Error(Token token, string message) {
            if (token.Type == TokenType.EOF) {
                Report(token.Line, " at end", message);
            } else {
                Report(token.Line, $" at '{token.Lexeme}'", message);
            }
        }

        internal void Error(InterpretException ex) => Report(ex.Token.Line, "", ex.Message);

        public void Output(string message) => _output?.WriteLine(message);

        private void Report(int line, string where, string message) {
            _output?.WriteError($"[line {line}] Error{where}: {message}");
        }

        public bool HadError => _output?.HasErrors ?? throw new NullReferenceException("_output is null, it really shouldn't be");
    }
}