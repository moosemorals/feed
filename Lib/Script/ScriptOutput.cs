using System.IO;

namespace Feed.Lib.Script {

    public class ScriptOutput {
        private readonly StringWriter _standard;
        private readonly StringWriter _errors;

        public ScriptOutput() {
            _errors = new();
            _standard = new();
        }

        public void WriteLine(string line) => _standard.WriteLine(line);

        public void WriteError(string line) => _errors.WriteLine(line);

        public string Standard => _standard.ToString();
        public string Errors => _errors.ToString();

        public bool HasErrors => _errors.GetStringBuilder().Length > 0;
    }
}