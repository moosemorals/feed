using System;

using Feed.Lib.Network;
using Feed.Models;

namespace Feed.Lib.Script {

    public class ScriptService : IPlugin {
        public Action<Command>? CommandSender { get; set; }

        public string Name => "Script";

        public bool HasJavascript => false;
        public bool IsEnabled => false;

        public bool Match(Command cmd) => cmd.To != null && cmd.To == Name;

        public void ExecuteCommand(Command cmd, Action<Command> SendReply) {

            throw new NotImplementedException();

            if (cmd.Payload.StartsWith("@script")) {
                string script = cmd.Payload.Substring("@script".Length);

                ScriptEngine engine = new ScriptEngine();

                ScriptOutput result = engine.Run(script);

            }
        }

    }
}