using System.Collections.Generic;

namespace Feed.Lib.Script {
    internal abstract record Stmt {
        internal record Block(List<Stmt> Statements) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitBlock(this);
        }

        internal record Expression(Expr Expr) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitExpression(this);
        }

        internal record Function(Token Name, List<Token> Params, List<Stmt> Body) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitFunction(this);
        }

        internal record If(Expr Condition, Stmt ThenBranch, Stmt? ElseBranch) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitIf(this);
        }

        internal record Print(Expr Expr) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitPrint(this);
        }

        internal record Return(Token Keyword, Expr? Value) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitReturn(this);
        }

        internal record Var(Token Name, Expr? Initializer) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitVar(this);
        }

        internal record While(Expr Condition, Stmt Body) : Stmt {
            internal override T Accept<T>(IVisitor<T> visitor) => visitor.VisitWhile(this);
        }

        internal interface IVisitor<T> {
            T VisitBlock(Block stmt);
            T VisitExpression(Expression stmt);
            T VisitFunction(Function stmt);
            T VisitIf(If stmt);
            T VisitPrint(Print stmt);
            T VisitVar(Var stmt);
            T VisitReturn(Return stmt);
            T VisitWhile(While stmt);
        }

        internal abstract T Accept<T>(IVisitor<T> visitor);
    }
}