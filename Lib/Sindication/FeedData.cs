using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;

namespace Feed.Lib.Sindication {
    public record FeedData(string Target) {

        public DateTimeOffset? LastFetch { get; set; }
        public SyndicationFeed? LastFeed { get; set; }

        public string TargetHash => Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(Target)));
    }
}
