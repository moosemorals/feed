using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel.Syndication;
using System.Net.Http;
using System.IO;
using System.Xml;
using Serilog;
using Feed.Lib.Cache;

namespace Feed.Lib.Sindication {
    public class FeedEngine {

        private readonly ILogger _log = Log.ForContext<FeedEngine>();
        private readonly IFetcher<SyndicationFeed> _fetcher;
        private readonly List<FeedData> _feeds = new();

        public FeedEngine(IFetcher<SyndicationFeed> fetcher) {
            _fetcher = fetcher;
            _fetcher.ResponseConverter = ParseFeed;
        }

        public void Subscribe(string target) {
            _log.Debug("Adding feed {feedTarget}", target);
            _feeds.Add(new FeedData(target));
        }

        public void Unsubscribe(string target) {
            _log.Debug("Removing feed {feedTarget}", target);
            FeedData? feed = _feeds.SingleOrDefault(f => f.Target == target);
            if (feed != null) {
                _feeds.Remove(feed);
            }
        }

        public IEnumerable<SyndicationFeed> FetchAllAsync() {
            Task.WaitAll(_feeds.Select(f => FetchAsync(f)).ToArray());
            return _feeds.Select(f => f.LastFeed).OfType<SyndicationFeed>();
        }

        private async Task FetchAsync(FeedData feed) {
            _log.Information("Fetching feed {feedTarget}", feed.Target);

            feed.LastFeed = await _fetcher.Fetch(feed.Target);
        }

        private SyndicationFeed ParseFeed(byte[] buffer) {
            using (XmlReader xml = XmlReader.Create(new MemoryStream(buffer))) {
                SyndicationFeed feed = SyndicationFeed.Load(xml);

                foreach (SyndicationItem item in feed.Items) {
                    if (item.SourceFeed == null) {
                        item.SourceFeed = feed;
                    }
                }

                return feed;
            }
        }
    }
}
