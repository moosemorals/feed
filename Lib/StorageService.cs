using System;

using Feed.Lib.Network;
using Feed.Lib.Stores;
using Feed.Models;

namespace Feed.Lib {

    public class StorageService : IPlugin {
        private readonly IMessageStore _store;

        public StorageService(IMessageStore store) => _store = store;

        public string Name => "Storage";

        public bool HasJavascript => false;

        public bool Match(Command cmd) => true;

        public void ExecuteCommand(Command cmd, Action<Command> SendReply) => _store.Add(cmd);
    }
}