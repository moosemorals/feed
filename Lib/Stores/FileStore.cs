using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Common.Lib;

using Feed.Models;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Serilog;

namespace Feed.Lib.Stores {

    public class FileStore : IMessageStore, IDisposable {

        private readonly BlockingCollection<Command> _queue = new();
        private readonly IConfiguration _config;
        private readonly Task _background;
        private readonly static ILogger _log = Log.ForContext<FileStore>();

        public FileStore(IConfiguration config) {
            _config = config;
            _background = Task.Run(Background);
        }

        private string SavePath => Path.Join(_config[Static.ConfigSharedFolder], "feedStore.json");

        public void Add(Command message) => _queue.Add(message);

        public void Dispose() {
            GC.SuppressFinalize(this);
            _background.Dispose();
            _queue.Dispose();
        }

        public IEnumerable<Command> Fetch() => LoadFromDisk(SavePath);

        private void Background() {
            while (true) {
                _log.Debug("Waiting for next message");
                Command msg = _queue.Take();
                _log.Debug("Saving message {websocketMessage} to {feedSavePath}", msg, SavePath);
                SaveToDisk(SavePath, msg);
            }
        }

        // Stream from disk
        private IEnumerable<Command> LoadFromDisk(string path) {
            _log.Debug("Loading messages from {feedSavePath}", SavePath);
            if (!File.Exists(path)) {
                _log.Debug("No file - no messages");
                yield break;
            }

            string? line;
            lock (_queue) {
                StreamReader f = new(path);
                try {
                    while ((line = f.ReadLine()) != null) {

                        JObject json = JObject.Parse(line);

                        Command? msg = json.ToObject<Command>();
                        if (msg == null) {
                            _log.Warning("Can't parse {rawCommand}", line);
                            continue;
                        }

                        // Update from version 2 to version 3
                        if (msg.Version == 2) {
                            msg = msg with {
                                Format = json["cmd"]?.ToObject<string>() ?? "text",
                                Version = 3,
                            };
                        } 

                        if (StartsWithNumber(msg.Id)) {
                            msg = msg with {
                                Id = $"c{msg.Id}",
                            };
                        }

                        yield return msg;
                    }
                } finally {
                    f.Close();
                }
            }

            yield break;
        }

        private static bool StartsWithNumber(string str) => str[0] >= '0' && str[0] <= '9';



        private void SaveToDisk(string path, Command message) {
            lock (_queue) {
                StreamWriter f = File.Exists(path) ?
                    File.AppendText(path)
                    : File.CreateText(path);

                f.WriteLine(JsonConvert.SerializeObject(message));
                f.Flush();
                f.Close();
            }
        }
    }
}