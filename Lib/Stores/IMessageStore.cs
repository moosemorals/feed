using System.Collections.Generic;

using Feed.Models;

namespace Feed.Lib.Stores {

    public interface IMessageStore {

        public void Add(Command message);

        public IEnumerable<Command> Fetch();
    }
}