using System.Collections.Generic;
using System.Collections.Immutable;

using Feed.Models;

namespace Feed.Lib.Stores {

    public class MemoryStore : IMessageStore {
        private readonly List<Command> _messages = new();

        public void Add(Command message) {
            lock (_messages) {
                _messages.Add(message);
            }
        }

        public IEnumerable<Command> Fetch() {
            lock (_messages) {
                return _messages.ToImmutableList();
            }
        }
    }
}