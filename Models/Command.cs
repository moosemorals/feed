using System;

using Feed.Lib;
using Feed.Lib.Network;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Feed.Models {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy), ItemNullValueHandling = NullValueHandling.Ignore)]
    public record Command(
        DateTimeOffset Created,
        string Id,
        string Format,
        string From,
        string? To,
        string Payload,
        long Version = Names.CommandVersion
        ) {
    }

    // Needed for hitsoric reasons
    public record WebsocketMessage(string Username, DateTimeOffset Created, string Payload);
}