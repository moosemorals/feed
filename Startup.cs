using System;
using System.Net.Http;
using System.ServiceModel.Syndication;

using Common.Lib;
using Common.Services;

using Feed.Lib;
using Feed.Lib.Cache;
using Feed.Lib.Network;
using Feed.Lib.Script;
using Feed.Lib.Sindication;
using Feed.Lib.Stores;
using Feed.Models;

using Lights.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Feed {

    public class Startup {
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration config, IWebHostEnvironment env) {
            _config = config;
            _env = env;
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddCommon(_config, _env);

            services.AddSingleton(new HttpClient());

            services.AddSingleton<CachedFetchService>();
            services.AddSingleton<EventSource>();
            services.AddSingleton<FeedEngine>();
            services.AddSingleton<HueService>();

            services.AddSingleton<IPlugin, LightService>(); 
            services.AddSingleton<IPlugin, StorageService>();
            services.AddSingleton<IPlugin, EchoService>();

            services.AddSingleton<IMessageStore, FileStore>();

            services.AddTransient<IFetcher<SyndicationFeed>, CachedObjectService<SyndicationFeed>>();
            services.AddTransient<WeatherService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime) {
            lifetime.ApplicationStopping.Register(() => {
                IMessageStore store = app.ApplicationServices.GetRequiredService<IMessageStore>();

                if (store is IDisposable d) {
                    d.Dispose();
                }
            });

            app.UseCommon("/", env, app => {
                FeedEngine e = app.ApplicationServices.GetRequiredService<FeedEngine>();

                e.Subscribe("https://utcc.utoronto.ca/~cks/space/blog/?atom");
                e.Subscribe("https://christine.website/blog.atom");
                e.Subscribe("https://xkcd.com/atom.xml");
                e.Subscribe("https://stackoverflow.com/feeds/user/195833");
                e.Subscribe("https://browser.engineering/rss.xml");
                e.Subscribe("https://lobste.rs/rss");
            });
        }
    }
}