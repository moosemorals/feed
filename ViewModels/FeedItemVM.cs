using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Feed.ViewModels {


    public record FeedVM(string Title, Uri Link, Uri? ImageUrl);

    public record FeedItemVM(FeedVM Parent, DateTimeOffset Timestamp, string Title, Uri Link) {

        public string Domain => Link.Host;

    }



}
