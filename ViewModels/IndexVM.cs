using System.Collections.Generic;

using Feed.Models;

namespace Feed.ViewModels {

    public class IndexVM {

        public IndexVM(string? userName, IEnumerable<Command> history, IDictionary<string, string> hashes) {
            History = history;

            if (userName == null) {
                UserName = "Guest";
                IsLoggedIn = false;
            } else {
                IsLoggedIn = true;
                UserName = userName;
            }
            FileHashes = hashes;
        }

        public IDictionary<string, string> FileHashes { get; init; }

        public IEnumerable<Command> History { get; init; }

        public string UserName { get; init; }

        public bool IsLoggedIn { get; init; }
    }
}