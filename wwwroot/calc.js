

const tokenType = {
    LEFT_PAREN: "LEFT_PAREN", RIGHT_PAREN: "RIGHT_PAREN", LEFT_BRACE: "LEFT_BRACE",
    RIGHT_BRACE: "RIGHT_BRACE", COMMA: "COMMA", DOT: "DOT", MINUS: "MINUS",
    PLUS: "PLUS", SEMICOLON: "SEMICOLON", SLASH: "SLASH", STAR: "STAR", BANG: "BANG",
    BANG_EQUAL: "BANG_EQUAL", EQUAL: "EQUAL", EQUAL_EQUAL: "EQUAL_EQUAL", GREATER: "GREATER",
    GREATER_EQUAL: "GREATER_EQUAL", LESS: "LESS", LESS_EQUAL: "LESS_EQUAL",
    IDENTIFIER: "IDENTIFIER", STRING: "STRING", NUMBER: "NUMBER", AND: "AND", CLASS: "CLASS",
    ELSE: "ELSE", FALSE: "FALSE", FUNC: "FUNC", FOR: "FOR", IF: "IF", NIL: "NIL",
    OR: "OR", PRINT: "PRINT", RETURN: "RETURN", SUPER: "SUPER", THIS: "THIS",
    TRUE: "TRUE", VAR: "VAR", WHILE: "WHILE", EOF: "EOF",
};

const exprTypes = {
    binary: "BINARY", literal: "LITERAL", variable: "VARIABLE", grouping: "GROUPING",
    call: "CALL", unary: "UNARY", assign: "ASSIGN", logical: "LOGICAL"
}


function scan(text) {

    const _keywords = {
        and: "AND", class: "CLASS", else: "ELSE",
        false: "FALSE", for: "FOR", func: "FUNC", if: "IF", nil: "NIL",
        or: "OR", print: "PRINT", return: "RETURN", super: "SUPER",
        this: "THIS", true: "TRUE", var: "VAR", while: "WHILE",
    }

    const _tokens = [];

    let _start = 0;
    let _current = 0;
    let _line = 0;

    while (!_isAtEnd()) {
        _start = _current;
        _scanToken();
    }

    _tokens.push({
        type: tokenType.EOF,
        lexeme: "",
        literal: null,
        line: _line
    });

    return _tokens;

    function _scanToken() {
        const c = _advance();

        switch (c) {
            case '(': _addToken(tokenType.LEFT_PAREN); break;
            case ')': _addToken(tokenType.RIGHT_PAREN); break;
            case '{': _addToken(tokenType.LEFT_BRACE); break;
            case '}': _addToken(tokenType.RIGHT_BRACE); break;
            case ',': _addToken(tokenType.COMMA); break;
            case '.': _addToken(tokenType.DOT); break;
            case '-': _addToken(tokenType.MINUS); break;
            case '+': _addToken(tokenType.PLUS); break;
            case ';': _addToken(tokenType.SEMICOLON); break;
            case '*': _addToken(tokenType.STAR); break;
            case '!':
                _addToken(_match('=') ? tokenType.BANG_EQUAL : tokenType.BANG);
                break;
            case '=':
                _addToken(_match('=') ? tokenType.EQUAL_EQUAL : tokenType.EQUAL);
                break;
            case '<':
                _addToken(_match('=') ? tokenType.LESS_EQUAL : tokenType.LESS);
                break;
            case '>':
                _addToken(_match('=') ? tokenType.GREATER_EQUAL : tokenType.GREATER);
                break;

            case '/':
                if (_match('/')) {
                    // Comment to EOL
                    while (!_isAtEnd && peek() != '\n') {
                        _advance();
                    }
                } else {
                    _addToken(tokenType.SLASH);
                }
                break;
            case ' ':
            case '\r':
            case '\t':
                // Ignorable whitespace
                break;

            case '\n':
                _line += 1;
                break;

            case '"': _string(); break;

            default:
                if (_isDigit(c)) {
                    _number();
                } else if (_isAlpha(c)) {
                    _identifier();
                } else {
                    // TODO: Error handling
                    console.log("unexpected char")
                }
                break;

        }
    }


    function _addToken(type, literal) {
        const lexeme = text.substring(_start, _current);
        _tokens.push({
            type: type,
            lexeme: lexeme,
            literal: literal,
            line: _line
        });
    }

    function _advance() {
        return text.charAt(_current++);
    }

    function _identifier() {
        while (_isAlphaNumeric(_peek())) {
            _advance();
        }

        const str = text.substring(_start, _current);
        if (_keywords[str]) {
            _addToken(_keywords[str])
        } else {
            _addToken(tokenType.IDENTIFIER)
        }
    }

    function _isAtEnd() {
        return _current >= text.length;
    }

    function _isAlpha(c) {
        return (c >= 'a' && c <= 'z')
            || (c >= 'A' && c <= 'Z')
            || c == '_';
    }

    function _isAlphaNumeric(c) {
        return _isAlpha(c) || _isDigit(c);
    }

    function _isDigit(c) {
        return c >= '0' && c <= '9';
    }

    function _match(expected) {
        if (_isAtEnd() || text.charAt(_current) != expected) {
            return false;
        }
        _current += 1;
        return true;
    }

    function _number() {
        while (_isDigit(_peek())) {
            _advance();
        }

        if (_peek() == '.' && _isDigit(_peekNext())) {
            _advance(); // to skip the dot
            while (_isDigit(peek())) {
                _advance();
            }
        }

        const str = text.substring(_start, _current);
        const val = parseFloat(str);
        if (!isNaN(val)) {
            _addToken(tokenType.NUMBER, val);
        } else {
            // TODO: Error handling
            console.error("Not a number", str, _line);
        }
    }

    function _peek() {
        return _isAtEnd() ? '\0' : text.charAt(_current);
    }

    function _peekNext() {
        if (_current + 1 >= text.length) {
            return '\0';
        }

        return text[_current + 1];
    }

    function _string() {
        while (!_isAtEnd && _peek() != '"') {
            if (_peek() == '\n') {
                _line += 1;
            }
            _advance();
        }

        if (_isAtEnd) {
            // TODO: Error handling
            console.error("String literal missing final '\"'.", _line);
            return;
        }

        _advance(); // eat trailing "

        const str = text.substring(_start + 1, _current - 1);
        _addToken(tokenType.STRING, str);
    }

}

function parse(tokens) {

    let _current = 0;

    function _parseBinary(next, type, ...types) {
        let expr = next();
        while (_match(...types)) {
            const op = _previous();
            const right = next();
            expr = {
                type: type,
                left: expr,
                op: op,
                right: right
            };
        }

        return expr;
    }

    const _factor = () => _parseBinary(_unary, exprTypes.binary, tokenType.SLASH, tokenType.STAR);
    const _term = () => _parseBinary(_factor, exprTypes.binary, tokenType.MINUS, tokenType.PLUS);
    const _comparison = () => _parseBinary(_term, exprTypes.binary, tokenType.GREATER, tokenType.GREATER_EQUAL, tokenType.LESS, tokenType.LESS_EQUAL)
    const _equality = () => _parseBinary(_comparison, exprTypes.binary, tokenType.BANG_EQUAL, tokenType.EQUAL_EQUAL);
    const _and = () => _parseBinary(_equality, exprTypes.logical, tokenType.AND);
    const _or = () => _parseBinary(_and, exprTypes.logical, tokenType.OR);


    return _expression();

    function _primary() {
        if (_match(tokenType.FALSE)) {
            return {
                type: exprTypes.literal,
                value: false
            };
        }
        if (_match(tokenType.TRUE)) {
            return {
                type: exprTypes.literal,
                value: true
            }
        }
        if (_match(tokenType.NIL)) {
            return {
                type: exprTypes.literal,
                value: true
            }
        }
        if (_match(tokenType.NUMBER, tokenType.STRING)) {
            return {
                type: exprTypes.literal,
                value: _previous().literal
            }
        }

        if (_match(tokenType.IDENTIFIER)) {
            return {
                type: exprTypes.variable,
                value: _previous()
            }
        }

        if (_match(tokenType.LEFT_PAREN)) {
            const expr = _expression();
            _consume(tokenType.RIGHT_PAREN, "Expected ')' after expression");
            return {
                type: exprTypes.grouping,
                expr: expr
            }
        }

        throw _error(_peek(), "Expected expression.");
    }

    // TODO: Call
    function _call() {
        return _primary();
    }

    function _unary() {
        if (_match(tokenType.BANG, tokenType.MINUS)) {
            const op = _previous()
            const right = _unary();
            return {
                type: exprTypes.unary,
                op: op,
                right: right
            }
        }
        return _call();
    }


    function _assignment() {
        let expr = _or();

        if (_match(tokenType.EQUAL)) {
            const equals = _previous();
            const value = _assignment();

            if (expr.type == exprTypes.variable) {
                const name = expr.name;
                return {
                    type: exprTypes.assign,
                    name: name,
                    value: value
                }
            }

            Error(equals, "Invalid assignment target.");
        }

        return expr;
    }

    function _expression() {
        return _assignment();
    }


    function _advance() {
        if (!_isAtEnd()) {
            _current += 1;
        }

        return _previous();
    }

    function _check(type) {
        return !_isAtEnd() && _peek().type == type;
    }

    function _consume(type, message) {
        if (_check(type)) {
            return _advance();
        }
        throw _error(_peek(), message);
    }

    function _error(token, message) {
        // TODO error handling
        return new Error(message);
    }

    function _isAtEnd() {
        return _peek().type == tokenType.EOF;
    }

    function _match(...types) {
        for (let i = 0; i < types.length; i += 1) {
            if (_check(types[i])) {
                _advance();
                return true;
            }
        }
        return false;
    }

    function _peek() {
        return tokens[_current];
    }

    function _previous() {
        return tokens[_current - 1];
    }

}

function evaluate(expression) {

    return _eval(expression); 

    function _evalBinary(expr) {

        const left = _eval(expr.left);
        const right = _eval(expr.right);

        switch (expr.op.type) {
            case tokenType.BANG_EQUAL:
                return !_areEqual(left, right);
            case tokenType.EQUAL_EQUAL:
                return _areEqual(left, right);
            case tokenType.GREATER:
                return left > right;
            case tokenType.GREATER_EQUAL:
                return left >= right;
            case tokenType.LESS:
                return left < right;
            case tokenType.LESS_EQUAL:
                return left <= right;
            case tokenType.MINUS:
                return left - right;
            case tokenType.SLASH:
                return left / right;
            case tokenType.STAR:
                return left * right;
            case tokenType.PLUS:
                return left + right;
        }
    }

    function _evalGrouping(expr) {
        return _eval(expr.expr);
    }

    function _evalLiteral(expr) {
        return expr.value;
    }

    function _evalLogical(expr) {
        const left = _eval(expr.left);

        if (expr.op.type == tokenType.OR) {
            if (_isTruthy(left)) {
                return left;
            }
        } else {
            if (!_isTruthy(left)) {
                return left;
            }
        }
        return _eval(expr.right);
    }

    function _evalUnary(expr) {
        const right = _eval(expr.right);

        switch (expr.op.type) {
            case tokenType.MINUS:
                return -right;
            case tokenType.BANG:
                return !_isTruthy(right);
        } 
    }

    function _eval(expr) {
        switch (expr.type) {
            case exprTypes.assign:
                return _evalAssign(expr);
            case exprTypes.binary:
                return _evalBinary(expr);
            case exprTypes.call:
                return _evalCall(expr);
            case exprTypes.grouping:
                return _evalGrouping(expr);
            case exprTypes.literal:
                return _evalLiteral(expr);
            case exprTypes.logical:
                return _evalLogical(expr);
            case exprTypes.unary:
                return _evalUnary(expr);
            case exprTypes.variable:
                return _evalVariable(expr);
        }
    }

    function _areEqual(a, b) {
        if (a == null && b == null) {
            return true;
        }
        if (a == null) {
            return false;
        }

        return a == b;
    }

    function _isTruthy(a) {
        if (a == null) {
            return false;
        }
        if (typeof a == "boolean") {
            return a;
        }
        return true;
    }

}

export function calc(text) {

    const tokens = scan(text);

    const expr = parse(tokens);

    return evaluate(expr);

}