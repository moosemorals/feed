
import { $, $$, tag } from './_content/Common/static/common.js';

/**
 * Remove the current selection from the DOM, wrap it
 * in a new element, and then put it back.
 * @param {string} tagName The name of the wrapping element
 */
function wrapSelectionInTag(parent, tagName, options) {
    const selection = document.getSelection();

    function _wrap(n, o, el) {
        const t = tag(n, o);
        el.parentNode.insertBefore(t, el);
        t.appendChild(el);
        return t;
    }

    if (selection.rangeCount === 0) {
        return;
    }

    const range = selection.getRangeAt(0);

    // Check the selection is inside the editor
    if (!parent.contains(range.commonAncestorContainer)) {
        return;
    }

    if (range.startContainer == range.endContainer) {
        // Start/end are the same node
        // so split the text and wrap the node

        const el = range.startContainer.splitText(range.startOffset);
        el.splitText(range.endOffset);
        _wrap(tagName, options, el);
    } else {
        // Need to walk up and down the tree to insert nodes
        let el = range.startContainer;
        outer: while (true) {

            if (el.nodeType == Node.ELEMENT_NODE && el.firstChild) {
                el = el.firstChild;
            }
            if (el.nodeType == Node.TEXT_NODE) {
                let done = false;
                if (el == range.startContainer) {
                    el = el.splitText(range.startOffset);
                }
                if (el == range.endContainer) {
                    done = true;
                    el.splitText(range.endOffset);
                }
                el = _wrap(tagName, options, el);
                if (done) {
                    break;
                }
            }

            while (!el.nextSibling) {
                el = el.parentNode;
                if (el.nodeType == Node.ELEMENT_NODE && el == range.endContainer) {
                    break outer;
                }
            }
            el = el.nextSibling;
        }
    }
}


function startList(e) {



}

function buildLink(parent) {
    const link = prompt("Give me a link");
    if (link == null || link == "") {
        return;
    }

    wrapSelectionInTag(parent, "a", {
        href: link,
        rel: "noopener noreferrer",
        target: "_blank"
    });
}

function keyHandlerMaker(editArea, doPost) {
    return function (e) {

        $("#output").scroll(0, $("#output").scrollHeight);

        if (e.isComposing || e.keyCode === 229) {
            return;
        }

        if (e.ctrlKey) {
            if (e.repeat) {
                return;
            }

            switch (e.key) {
                case "b":
                    wrapSelectionInTag(editArea, "strong");
                    e.preventDefault();
                    break;
                case "i":
                    wrapSelectionInTag(editArea, "em");
                    e.preventDefault();
                    break;
            }
            return;
        }

        switch (e.key) {
            case "Enter":
                if ($('#autoPost').checked) {
                    doPost(e);
                }
                break;
        }
    }
}

export function editor(doPost) {

    const editArea = $(".edit-area");

    editArea.addEventListener("keydown", keyHandlerMaker(editArea, doPost));

    $("#cmdStrong").addEventListener("click", () => wrapSelectionInTag(editArea, "strong"));
    $("#cmdEm").addEventListener("click", () => wrapSelectionInTag(editArea, "em"));
    $("#cmdCode").addEventListener("click", () => wrapSelectionInTag(editArea, "code"));
    $("#cmdList").addEventListener("click", startList);
    $("#cmdLink").addEventListener("click", () => buildLink(editArea));

    requestAnimationFrame(() => {
        editArea.focus();
    });
}

