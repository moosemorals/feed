import { $, $$, append, getText, tag, toNode } from '/_content/Common/static/common.js'
import { editor } from './edit.js';
import { calc } from './calc.js';

let messageCounter = 0;

/**
 * Compare two dates with a given precision
 * @param {Date} d1
 * @param {Date} d2
 * @param {"year"|"month"|"day"|"hour"|"minute"|"second"|"ms"} precision
 */
function compareDates(d1, d2, precision) {
    const prec = { "year": 0, "month": 1, "day": 2, "hour": 3, "minute": 4, "second": 5, "ms": 6 };
    const gets = ["getFullYear", "getMonth", "getDate", "getHours", "getMinutes", "getSeconds", "getMilliseconds"];

    for (let i = 0; i <= prec[precision]; i += 1) {
        const get = gets[i];

        if (Date.prototype[get].apply(d1) != Date.prototype[get].apply(d2)) {
            return false;
        }
    }

    return true;
}
const sameDay = (d1, d2) => compareDates(d1, d2, "day");
const sameMinute = (d1, d2) => compareDates(d1, d2, "minute");

function formatISODateTime(date) {
    // From https://stackoverflow.com/a/17415677/195833
    var tzo = -date.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function (num) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };

    return date.getFullYear() +
        '-' + pad(date.getMonth() + 1) +
        '-' + pad(date.getDate()) +
        'T' + pad(date.getHours()) +
        ':' + pad(date.getMinutes()) +
        ':' + pad(date.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
}

function formatISODate(date) {
    const iso = formatISODateTime(date);

    return iso.substring(0, iso.indexOf('T'));
}


function formatTimestamp(date) {
    const pad = n => n.toString().padStart(2, '0');
    return `${pad(date.getHours())}:${pad(date.getMinutes())}`;
}

/**
 * Convert a string into a HTML Element ready to display
 * @param {string} command
 * @returns {HTMLDivElement} display element
 */
function formatMessage(command) {
    function _divToMsg(div) {
        if (div != null) {
            return {
                created: new Date(div.dataset.created),
                from: div.dataset.from,
                payload: $(div, ".payload").innerHTML
            }
        } else {
            return {
                created: new Date(0),
                payload: "",
                from: "System"
            };
        }
    }

    function _isHidden(el) {
        return el && el.classList.contains("hidden");
    }

    const history = $$("#output .message");
    const prevDiv = history[history.length - 1];
    const prev = _divToMsg(prevDiv);

    const p = tag("span", "payload");
    const t = tag("span", "created", formatTimestamp(command.created));
    const m = tag("div", {
        id: command.id,
        class: "message",
        "data-created": formatISODateTime(command.created),
        "data-from": command.from
    });


    if (command.format == "html") {
        p.innerHTML = command.payload;
    } else {
        p.appendChild(toNode(command.payload));
    }

    function _isDC(msg) {
        return isConnectMessage(msg) || isDisconnectMessage(msg);
    }

    if (!_isHidden(prevDiv) && sameMinute(command.created, prev.created)) {
        t.classList.add("hidden");
    }

    if (!sameDay(command.created, prev.created)) {

        const f = document.createDocumentFragment();
        f.appendChild(tag("div", "daybreak", tag("hr"), formatISODate(command.created)));
        f.appendChild(m);

        append(m, t, p);
        return f;
    }

    return append(m, t, p);
}

function isDisconnectMessage(msg) {
    return msg.from == "System" && msg.payload.endsWith("Disconnected")
}

function isConnectMessage(msg) {
    return msg.from == "System" && msg.payload.endsWith("Connected")
}

function loadHistory() {
    const json = $("#history").innerText;
    const history = JSON.parse(json);

    for (let i = 0; i < history.length; i += 1) {
        history[i].created = new Date(history[i].created);
    }

    return history;
}

function showHistory(history) {
    for (let i = 0; i < history.length; i += 1) {
        writeOutput(formatMessage(history[i]))
    }
}

function getUserName() {
    const g = $("#greeting");
    if (g == null) {
        return null;
    }

    return g.dataset.name;
}

function newMessage(payload, from = getUserName(), to) {
    const now = new Date();

    return {
        created: now,
        id: `c${now.getTime()}.${messageCounter++}`,
        from: from,
        to: to,
        payload: payload,
        version: 3
    }
}

/**
 * Send message to backend
 * @param {string|HTMLElement} message
 */
function sendMessage(message) {
    fetch("/events", {
        method: "POST",
        body: JSON.stringify(message),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: 'same-origin',
        mode: 'same-origin'
    });
}

function clockTick() {
    const now = new Date();
    const ts = formatTimestamp(now);
    $("#clock").innerText = ts;

    const delay = 1000 - now.getMilliseconds();
    setTimeout(clockTick, delay);
}

/**
 * Write a string to output
 * @param {HTMLElement} el
 */
function writeOutput(el) {
    const output = $("#output");
    output.appendChild(el);
    output.scroll(0, output.scrollHeight);
}

function handleCommand(el) {

    // Strip leading '/'
    const raw = el.innerText.substring(1);

    while (el.firstChild) {
        el.removeChild(el.firstChild);
    }

    // Get command name
    const spaceIndex = raw.indexOf(" ");
    const name = spaceIndex === -1 ? raw : raw.substring(0, spaceIndex);

    // TODO: check if name is known
    // TODO: Handle local commands

    const cmd = newMessage(raw.substring(spaceIndex + 1), getUserName(), name.toLowerCase());
    cmd.format = "text";
    return cmd;
}

function simplePost(el) {
    let body = "<div class='outer'>";

    while (el.firstChild) {
        const child = el.removeChild(el.firstChild);
        if (child.nodeType == Node.TEXT_NODE) {
            body += child.nodeValue;
        } else {
            body += child.outerHTML;
        }
    }

    body += "</div>"

    const cmd = newMessage(body, getUserName());
    cmd.format = "html";
    return cmd;
}

function doPost(e) {
    e.preventDefault();
    const editArea = $(".edit-area");

    let cmd;

    // Deal wth commands (that start with '/')
    if (editArea.innerText.substring(0, 1) === '/') {
        cmd = handleCommand(editArea);
    } else {
        cmd = simplePost(editArea);
    }

    sendMessage(cmd);
}

let hashes;
function loadHashes() {
    hashes = JSON.parse($("#hashes").innerText);
}

function addHash(filename) {
    if (!hashes || !(filename in hashes)) {
        return filename;
    }
    return `${filename}?v=${hashes[filename]}`;
}

function connect(target) {
    const sw = new SharedWorker(addHash("sharedworker.js"));

    sw.onerror = e => console.error("SW error", e);
    sw.port.onmessage = receiveMessage;
    sw.port.start();

}

function receiveMessage(e) {
    console.log("Message received", e);

    const command = JSON.parse(e.data);
    command.created = new Date(command.created);

    if ($("#" + CSS.escape(command.id))) {
        console.log("Already showing message with id ", command.id);
        return;
    }

    writeOutput(formatMessage(command));
}


function init() {
    clockTick();

    // This isn't really security, it's just cleaner not
    // to bother connecting if the user isn't logged in.
    const u = getUserName();
    if (u != null) {
        loadHashes();
        connect()
        editor(doPost);

        $("#cmdSave").addEventListener("click", doPost);
        showHistory(loadHistory());
    }
}

window.addEventListener("DOMContentLoaded", init);