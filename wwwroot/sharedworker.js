﻿/*
  Service worker to multiplex connections from an EventSource
  It receives the events and sends them to any connected clients
  Clients are responsible for sending their own messages to the back end
*/
 
let eventSource;
let esConnected = false;
let backoff = 125;
const url = "/events/";
const ports = [];

function broadcast(message) {
    for (let i = 0; i < ports.length; i += 1) {
        const port = ports[i];
        try {
            port.postMessage(message);
        } catch (error) {
            console.error("Problem posting a message to a client, assuming it's closed", error);
            ports.splice(i, 1);
            i -= 1;
        }
    }
}

function connect() {
    console.log("EventSource: Connecting")

    if (eventSource != null && eventSource.readyState <= 1) {
        // already connected
        esConnected = true;
        return;
    }

    eventSource = new EventSource(url, {withCredentials: true});
    eventSource.onopen = esOpen;
    eventSource.onmessage = esMessage;
    eventSource.onerror = esError;
}

function esOpen(e) {
    console.log("EventSource: connected");
    backoff = 125;
}

function esMessage(e) {
    broadcast(e.data);
}

function esError(e) {
    console.log(`EventSource: closed, waiting ${backoff}ms and reconnecting`)
    setTimeout(() => {
        backoff *= 2;
        if (backoff > 15 * 1000) {
            backoff = 15 * 1000;
        }
        connect();
    }, backoff);
}

// A client has connected 
self.onconnect = e => {
    if (!esConnected) {
        connect();
    }

    const port = e.ports[0];
    ports.push(port);

    // Note, messages from the client are ignored
};
